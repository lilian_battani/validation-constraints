package net.tncy.validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ISBNValidator.class)
@Documented
public @interface ISBN {
    String message() default "{net.tncy.validator.constraints.books.ISBN}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
